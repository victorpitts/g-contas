<?php
function nome_ps_seg_perfil($id_ps_seg_perfil){
	include '../com.php';
	include_once 'ps_modulo_get_nome.php';
	include_once 'ps_perfil_get_nome.php';
	include_once 'ps_menu_get_nome.php';
	$sql_get = $c_pdo->prepare("SELECT * FROM `ps_seg_perfil` WHERE id_ps_seg_perfil  = :id_ps_seg_perfil

	");
	$sql_get->execute(array('id_ps_seg_perfil' => $id_ps_seg_perfil));
		if($sql_get){
		$i=0;
		$resp = array('respo' => array());
		while($sql_resp = $sql_get->fetchObject()){
			$resp['respo'][$i]['id_ps_seg_perfil'] = $sql_resp->id_ps_seg_perfil;
			$resp['respo'][$i]['id_uk_ps_seg_perfil'] = $sql_resp->id_uk_ps_seg_perfil;
			$resp['respo'][$i]['ver_ps_seg_perfil'] = $sql_resp->ver_ps_seg_perfil;
			$resp['respo'][$i]['insert_ps_seg_perfil'] = $sql_resp->insert_ps_seg_perfil;
			$resp['respo'][$i]['select_ps_seg_perfil'] = $sql_resp->select_ps_seg_perfil;
			$resp['respo'][$i]['update_ps_seg_perfil'] = $sql_resp->update_ps_seg_perfil;
			$resp['respo'][$i]['delete_ps_seg_perfil'] = $sql_resp->delete_ps_seg_perfil;
			$resp['respo'][$i]['exportar_ps_seg_perfil'] = $sql_resp->exportar_ps_seg_perfil;
			$resp['respo'][$i]['ps_modulo'] = $sql_resp->ps_modulo;
			$resp['respo'][$i]['ps_perfil'] = $sql_resp->ps_perfil;
			$resp['respo'][$i]['ps_menu'] = $sql_resp->ps_menu;
			$i++;
		}
		return $resp;
		$sql_get = null;
	}	
}
function perfil_ps_seg_perfil($ps_perfil){
	include '../com.php';
	$sql_get = $c_pdo->prepare("SELECT * FROM `ps_seg_perfil` WHERE ps_perfil  = :ps_perfil");
	$sql_get->execute(array('ps_perfil' => $ps_perfil));
		if($sql_get){
		$i=0;
		$resp = array('respo' => array());
		while($sql_resp = $sql_get->fetchObject()){
			$resp['respo'][$i]['id_ps_seg_perfil'] = $sql_resp->id_ps_seg_perfil;
			$resp['respo'][$i]['id_uk_ps_seg_perfil'] = $sql_resp->id_uk_ps_seg_perfil;
			$resp['respo'][$i]['ver_ps_seg_perfil'] = $sql_resp->ver_ps_seg_perfil;
			$resp['respo'][$i]['insert_ps_seg_perfil'] = $sql_resp->insert_ps_seg_perfil;
			$resp['respo'][$i]['select_ps_seg_perfil'] = $sql_resp->select_ps_seg_perfil;
			$resp['respo'][$i]['update_ps_seg_perfil'] = $sql_resp->update_ps_seg_perfil;
			$resp['respo'][$i]['delete_ps_seg_perfil'] = $sql_resp->delete_ps_seg_perfil;
			$resp['respo'][$i]['exportar_ps_seg_perfil'] = $sql_resp->exportar_ps_seg_perfil;
			$resp['respo'][$i]['ps_modulo'] = $sql_resp->ps_modulo;
			$resp['respo'][$i]['ps_perfil'] = $sql_resp->ps_perfil;
			$resp['respo'][$i]['ps_menu'] = $sql_resp->ps_menu;
			$i++;
		}
		return $resp;
		$sql_get = null;
	}	
}
function perfil_menu_ps_seg_perfil($ps_perfil, $ps_menu){
	include '../com.php';
	$sql_get = $c_pdo->prepare("SELECT * FROM `ps_seg_perfil` WHERE ps_perfil  = :ps_perfil AND ps_menu = :ps_menu");
	$sql_get->execute(array('ps_perfil' => ''.$ps_perfil.'', 'ps_menu' => ''.$ps_menu.''));
	if($sql_get){
		$i=0;
		$resp = array('respo' => array());
		while($sql_resp = $sql_get->fetchObject()){
			$resp['respo'][$i]['id_ps_seg_perfil'] = $sql_resp->id_ps_seg_perfil;
			$resp['respo'][$i]['id_uk_ps_seg_perfil'] = $sql_resp->id_uk_ps_seg_perfil;
			$resp['respo'][$i]['ver_ps_seg_perfil'] = $sql_resp->ver_ps_seg_perfil;
			$resp['respo'][$i]['insert_ps_seg_perfil'] = $sql_resp->insert_ps_seg_perfil;
			$resp['respo'][$i]['select_ps_seg_perfil'] = $sql_resp->select_ps_seg_perfil;
			$resp['respo'][$i]['update_ps_seg_perfil'] = $sql_resp->update_ps_seg_perfil;
			$resp['respo'][$i]['delete_ps_seg_perfil'] = $sql_resp->delete_ps_seg_perfil;
			$resp['respo'][$i]['exportar_ps_seg_perfil'] = $sql_resp->exportar_ps_seg_perfil;
			$resp['respo'][$i]['ps_modulo'] = $sql_resp->ps_modulo;
			$resp['respo'][$i]['ps_perfil'] = $sql_resp->ps_perfil;
			$resp['respo'][$i]['ps_menu'] = $sql_resp->ps_menu;
			$i++;
		}
		return $resp;
		$sql_get = null;
	}	
}


?>