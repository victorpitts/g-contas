<?php
header('Access-Control-Allow-Origin: *');
if(!empty($_POST)){
	if(isset($_POST)){
		include '../com.php';
		extract($_POST);
		include_once 'ps_usuario_get_cook.php';
		$id_uk_ps_menu = validacao_uk('ps_menu', 'id_uk_ps_menu');
		$valid_t = valid_time($cook);		
		$usuario_ = get_user_cook($cook);			
		$ps_usuario = $usuario_['respo'][0]['id_uk_ps_usuario'];
		if($valid_t){
			if((empty($id_ps_menu))&&(empty($id_uk_ps_menu))&&(empty($ps_menu))&&(empty($nome_ps_menu))&&(empty($ps_icone))&&(empty($funcao_ps_menu))&&(empty($ordem_ps_menu))&&(empty($descricao_ps_menu))&&(empty($ativo_ps_menu))&&(empty($data_ps_menu))&&(empty($hora_ps_menu))&&(empty($ps_modulo))){

				$sql_get = $c_pdo->prepare("SELECT * FROM `ps_menu` WHERE ps_usuario  = :ps_usuario");
				$sql_get_arr = ['ps_usuario' => $ps_usuario];
				$sql_get->execute($sql_get_arr);
				
			}else{	
				$sql_get = $c_pdo->prepare("SELECT * FROM `ps_menu` WHERE (id_ps_menu  = :id_ps_menu OR id_uk_ps_menu  = :id_uk_ps_menu OR ps_menu  = :ps_menu OR nome_ps_menu  = :nome_ps_menu OR ps_icone  = :ps_icone OR funcao_ps_menu  = :funcao_ps_menu OR ordem_ps_menu  = :ordem_ps_menu OR descricao_ps_menu  = :descricao_ps_menu OR ativo_ps_menu  = :ativo_ps_menu OR data_ps_menu  = :data_ps_menu OR hora_ps_menu  = :hora_ps_menu OR ps_modulo  = :ps_modulo) AND ps_usuario  = :ps_usuario");

				$sql_get_arr = ['id_ps_menu' => $id_ps_menu, 'id_uk_ps_menu' => $id_uk_ps_menu, 'ps_menu' => $ps_menu, 'nome_ps_menu' => $nome_ps_menu, 'ps_icone' => $ps_icone, 'funcao_ps_menu' => $funcao_ps_menu, 'ordem_ps_menu' => $ordem_ps_menu, 'descricao_ps_menu' => $descricao_ps_menu, 'ativo_ps_menu' => $ativo_ps_menu, 'data_ps_menu' => $data_ps_menu, 'hora_ps_menu' => $hora_ps_menu, 'ps_modulo' => $ps_modulo, 'ps_usuario' => $ps_usuario];

				$sql_get->execute($sql_get_arr);
			}
			if($sql_get){
				$i=0;
				
				while($sql_resp = $sql_get->fetchObject()){
					$resp['respo'][$i]['id_ps_menu'] = $sql_resp->id_ps_menu;
					$resp['respo'][$i]['id_uk_ps_menu'] = $sql_resp->id_uk_ps_menu;
					$resp['respo'][$i]['ps_menu'] = $sql_resp->ps_menu;
					$resp['respo'][$i]['nome_ps_menu'] = utf8_encode($sql_resp->nome_ps_menu);
					$resp['respo'][$i]['ps_icone'] = $sql_resp->ps_icone;
					$resp['respo'][$i]['funcao_ps_menu'] = $sql_resp->funcao_ps_menu;
					$resp['respo'][$i]['ordem_ps_menu'] = $sql_resp->ordem_ps_menu;
					$resp['respo'][$i]['descricao_ps_menu'] = $sql_resp->descricao_ps_menu;
					$resp['respo'][$i]['ativo_ps_menu'] = $sql_resp->ativo_ps_menu;
					$data_ps_menu_ = explode('-', $sql_resp->data_ps_menu);
					$sql_resp->data_ps_menu = $data_ps_menu_[2].'/'.$data_ps_menu_[1].'/'.$data_ps_menu_[0];
					$resp['respo'][$i]['data_ps_menu'] = $sql_resp->data_ps_menu;
					$resp['respo'][$i]['hora_ps_menu'] = $sql_resp->hora_ps_menu;
					$resp['respo'][$i]['ps_modulo'] = $sql_resp->ps_modulo;
					//$nome_ps_modulo = nome_ps_modulo($sql_resp->ps_modulo);
					//$resp['respo'][$i]['ps_modulo_nm'] = $nome_ps_modulo['respo'][0]['nome_ps_modulo'];
					$resp['respo'][$i]['ps_usuario'] = $sql_resp->ps_usuario;
					$i++;
			}
			echo json_encode($resp);
			$sql_get = null;
			}	
		}
	}
}
?>