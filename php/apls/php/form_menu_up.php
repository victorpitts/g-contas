<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8');
if(!empty($_POST)){
	if(isset($_POST)){
		include 'pad_com.php';
		extract($_POST);		
		include_once '../../ps_usuario_get_cook.php';
		$valid_t = valid_time($cook);		
		$usuario_ = get_user_cook($cook);
		$usuario = $usuario_['respo'][0]['id_uk_ps_usuario'];
		if($valid_t != 'false'){
			 $c_pdo = pad_com();
			 $data_ps_menu = date('Y-m-d');
	     	 $hora_ps_menu = date('H:i:s');
     		 $sql_up_arr = ['id_ps_menu' => $id_ps_menu, 'id_uk_ps_menu' => $id_uk_ps_menu, 'ps_menu' => $ps_menu, 'nome_ps_menu' => utf8_decode($nome_ps_menu), 'ps_icone' => $ps_icone, 'funcao_ps_menu' => $funcao_ps_menu, 'ordem_ps_menu' => $ordem_ps_menu, 'descricao_ps_menu' => utf8_decode($descricao_ps_menu), 'ativo_ps_menu' => $ativo_ps_menu, 'data_ps_menu' => $data_ps_menu, 'hora_ps_menu' => $hora_ps_menu, 'ps_modulo' => $ps_modulo, 'ps_usuario' => $ps_usuario, 'id_ps_menu' => $id_ps_menu];
	$sql_up = $c_pdo->prepare("UPDATE ps_menu SET id_ps_menu = :id_ps_menu, id_uk_ps_menu = :id_uk_ps_menu, ps_menu = :ps_menu, nome_ps_menu = :nome_ps_menu, ps_icone = :ps_icone, funcao_ps_menu = :funcao_ps_menu, ordem_ps_menu = :ordem_ps_menu, descricao_ps_menu = :descricao_ps_menu, ativo_ps_menu = :ativo_ps_menu, data_ps_menu = :data_ps_menu, hora_ps_menu = :hora_ps_menu, ps_modulo = :ps_modulo, ps_usuario = :ps_usuario WHERE id_ps_menu = :id_ps_menu");
	$sql_up->execute($sql_up_arr);
	if($sql_up){
				$id_uk_ps_usuario = $usuario_['respo'][0]['id_uk_ps_usuario'];
				$email_ps_usuario = $usuario_['respo'][0]['email_ps_usuario'];
				$tabela_ps_log = 'ps_menu';
				$colunas_se_ps_log = '';
				$colunas_in_ps_log = $sql_up_arr;
				$acao_ps_log = 'UP';
				log_user($id_uk_ps_usuario,$email_ps_usuario,$tabela_ps_log,json_encode($colunas_se_ps_log),json_encode($colunas_in_ps_log),$acao_ps_log);		
				echo json_encode('ALTERADO COM SUCESSO');
				$sql_up = null;
			}
				}
	}
}
?>