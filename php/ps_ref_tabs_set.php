<?php
header('Access-Control-Allow-Origin: *');
function cad_cols($id_uk_ps_ref_cols, $nome_coluna_ps_ref_cols, $alias_ps_ref_cols, $ps_ref_tabs, $tipo_ps_ref_cols, $tabela_fk_ps_ref_cols, $coluna_fk_ps_ref_cols, $data_ps_ref_cols, $hora_ps_ref_cols, $ps_usuario, $tipo_db_ps_ref_cols, $tamanho_ps_ref_cols, $req_ps_ref_cols, $ps_conexoes){
	
	include '../com.php';
	
		$sql_set = $c_pdo->prepare("INSERT INTO `ps_ref_cols`(`id_uk_ps_ref_cols`,`nome_coluna_ps_ref_cols`,`alias_ps_ref_cols`,`ps_ref_tabs`,`tipo_ps_ref_cols`,`tabela_fk_ps_ref_cols`,`coluna_fk_ps_ref_cols`,`data_ps_ref_cols`,`hora_ps_ref_cols`,`ps_usuario`,`tipo_db_ps_ref_cols`,`tamanho_ps_ref_cols`,`req_ps_ref_cols`,`ps_conexoes`) VALUES (:id_uk_ps_ref_cols,:nome_coluna_ps_ref_cols,:alias_ps_ref_cols,:ps_ref_tabs,:tipo_ps_ref_cols,:tabela_fk_ps_ref_cols,:coluna_fk_ps_ref_cols,:data_ps_ref_cols,:hora_ps_ref_cols,:ps_usuario,:tipo_db_ps_ref_cols,:tamanho_ps_ref_cols,:req_ps_ref_cols,:ps_conexoes)");
		$sql_set->execute(array('id_uk_ps_ref_cols' => $id_uk_ps_ref_cols, 'nome_coluna_ps_ref_cols' => $nome_coluna_ps_ref_cols, 'alias_ps_ref_cols' => $alias_ps_ref_cols, 'ps_ref_tabs' => $ps_ref_tabs, 'tipo_ps_ref_cols' => $tipo_ps_ref_cols, 'tabela_fk_ps_ref_cols' => $tabela_fk_ps_ref_cols, 'coluna_fk_ps_ref_cols' => $coluna_fk_ps_ref_cols, 'data_ps_ref_cols' => $data_ps_ref_cols, 'hora_ps_ref_cols' => $hora_ps_ref_cols, 'ps_usuario' => $ps_usuario, 'tipo_db_ps_ref_cols' => $tipo_db_ps_ref_cols, 'tamanho_ps_ref_cols' => $tamanho_ps_ref_cols, 'req_ps_ref_cols' => $req_ps_ref_cols, 'ps_conexoes' => $ps_conexoes));
	
	
}

function in_usuario($c_pdo){
		include_once 'ps_id_uk.php';
		$id_uk_ps_usuario = validacao_uk('ps_usuario', 'id_uk_ps_usuario');
		$sql_get = $c_pdo->prepare("SELECT * FROM `ps_usuario`");
		$sql_get->execute();
			if($sql_get){
				$i=0;
				while($sql_resp = $sql_get->fetchObject()){
					$id_uk_ps_usuario = $sql_resp->id_uk_ps_usuario;
					$i++;
				}
				if($i > 0){					
					return $id_uk_ps_usuario;
				}
				else{
					return '';
				}
			}
		
}
function in_perfil($c_pdo){
		
	$sql_get = $c_pdo->prepare("SELECT * FROM `ps_perfil`");
	$sql_get->execute();
	if($sql_get){
		$i=0;
		while($sql_resp = $sql_get->fetchObject()){
			$res = $sql_resp->id_uk_ps_perfil;
			$i++;
		}
		if($i > 0){					
			return $res;
		}
		else{
			return '';
		}
		
	}
}
function in_usuario_perfil($c_pdo, $ps_modulo, $ps_usuario, $ps_perfil){
		include_once 'ps_id_uk.php';
		$id_uk_ps_usuario_perfil = validacao_uk('ps_usuario_perfil', 'id_uk_ps_usuario_perfil');
		$sql_get = $c_pdo->prepare("SELECT * FROM `ps_usuario_perfil`");
		$sql_get->execute();
			if($sql_get){
				$i=0;
				while($sql_resp = $sql_get->fetchObject()){
					$i++;
				}
				if($i > 0){					
				}
				else{
					$sql_set = $c_pdo->prepare("INSERT INTO `ps_usuario_perfil`(`id_uk_ps_usuario_perfil`,`ps_usuario`,`ps_perfil`,`ps_modulo`) VALUES (:id_ps_usuario_perfil,:id_uk_ps_usuario_perfil,:ps_usuario,:ps_perfil,:ps_modulo)");
					$sql_set->execute(array('id_uk_ps_usuario_perfil' => $id_uk_ps_usuario_perfil, 'ps_usuario' => $ps_usuario, 'ps_perfil' => $ps_perfil, 'ps_modulo' => $ps_modulo));
	
				}
				return $id_uk_ps_usuario;
			}
			else{
				return "";
			}
		
}	
function in_seg_perfil($c_pdo, $ps_perfil, $ps_modulo, $ps_menu){

		include_once 'ps_id_uk.php';
		$id_uk_ps_seg_perfil = validacao_uk('ps_seg_perfil', 'id_uk_ps_seg_perfil');
		$sql_get = $c_pdo->prepare("SELECT * FROM `ps_seg_perfil` WHERE ps_modulo = :ps_modulo AND ps_perfil = :ps_perfil AND ps_menu = :ps_menu");
		$sql_get->execute(array('ps_modulo' => $ps_modulo, 'ps_perfil' => $ps_perfil, 'ps_menu' => $ps_menu));
			if($sql_get){
				$i=0;
				while($sql_resp = $sql_get->fetchObject()){
					$i++;
				}
				if($i > 0){					
				}
				else{
					
					$ver_ps_seg_perfil = "1";
					$insert_ps_seg_perfil = "1";
					$select_ps_seg_perfil = "1";
					$update_ps_seg_perfil = "1";
					$delete_ps_seg_perfil = "1";
					$exportar_ps_seg_perfil = "1";
					
					$sql_set = $c_pdo->prepare("INSERT INTO `ps_seg_perfil`(`id_uk_ps_seg_perfil`,`ver_ps_seg_perfil`,`insert_ps_seg_perfil`,`select_ps_seg_perfil`,`update_ps_seg_perfil`,`delete_ps_seg_perfil`,`exportar_ps_seg_perfil`,`ps_modulo`,`ps_perfil`,`ps_menu`) VALUES (:id_uk_ps_seg_perfil,:ver_ps_seg_perfil,:insert_ps_seg_perfil,:select_ps_seg_perfil,:update_ps_seg_perfil,:delete_ps_seg_perfil,:exportar_ps_seg_perfil,:ps_modulo,:ps_perfil,:ps_menu)");
					$sql_set->execute(array('id_uk_ps_seg_perfil' => $id_uk_ps_seg_perfil, 'ver_ps_seg_perfil' => $ver_ps_seg_perfil, 'insert_ps_seg_perfil' => $insert_ps_seg_perfil, 'select_ps_seg_perfil' => $select_ps_seg_perfil, 'update_ps_seg_perfil' => $update_ps_seg_perfil, 'delete_ps_seg_perfil' => $delete_ps_seg_perfil, 'exportar_ps_seg_perfil' => $exportar_ps_seg_perfil, 'ps_modulo' => $ps_modulo, 'ps_perfil' => $ps_perfil, 'ps_menu' => $ps_menu));

	
				}
			}

}
if(!empty($_POST)){
	if(isset($_POST)){
		include '../com.php';
		//var_dump($_POST);
		extract($_POST);

		include 'usuario_get3.php';
	
		include_once 'ps_menu_get_nome.php';
		
		include_once 'ps_id_uk.php';

		$id_uk_ps_ref_tabs = validacao_uk('ps_ref_tabs', 'id_uk_ps_ref_tabs');
		
		$id_uk_ps_menu = validacao_uk('ps_menu', 'id_uk_ps_menu');
		
		$v = usuario_perfil_acesso_n2_exec($cook, $funcao_exe);
		
		$usuario_ = cook2($cook);	
		
		$ps_usuario = (int)$usuario_['respo'][0]['usuario'];
		



		if($v != "n"){
			
			if($tp == "1"){
				
				
			$sql_get_uni = $c_pdo->prepare("SELECT * FROM `ps_menu` WHERE (ps_menu  = :ps_menu AND nome_ps_menu  = :nome_ps_menu AND ps_icone  = :ps_icone AND funcao_ps_menu  = :funcao_ps_menu AND ordem_ps_menu  = :ordem_ps_menu AND ps_modulo  = :ps_modulo) AND ps_usuario  = :ps_usuario");

			$sql_get_uni->execute(array('id_ps_menu' => $id_ps_menu, 'id_uk_ps_menu' => $id_uk_ps_menu, 'ps_menu' => $ps_menu, 'nome_ps_menu' => $nome_ps_menu, 'ps_icone' => $ps_icone, 'funcao_ps_menu' => $funcao_ps_menu, 'ordem_ps_menu' => $ordem_ps_menu, 'descricao_ps_menu' => $descricao_ps_menu, 'ativo_ps_menu' => $ativo_ps_menu, 'data_ps_menu' => $data_ps_menu, 'hora_ps_menu' => $hora_ps_menu, 'ps_modulo' => $ps_modulo, 'ps_usuario' => $ps_usuario));


				if($sql_get_uni){
					
				
					
					
					
					$j=0;
				while($sql_resp_uni = $sql_get_uni->fetchObject()){
					$j++;	
				}
				if($j > 0){
					
				}
				else{			
				
					$data_ps_menu = date('Y-m-d');
						
					$hora_ps_menu = date('H:i:s');
						
					$sql_set1 = $c_pdo->prepare("INSERT INTO `ps_menu`(`id_uk_ps_menu`,`ps_menu`,`nome_ps_menu`,`ps_icone`,`funcao_ps_menu`,`ordem_ps_menu`,`descricao_ps_menu`,`ativo_ps_menu`,`data_ps_menu`,`hora_ps_menu`,`ps_modulo`,`ps_usuario`) VALUES (:id_uk_ps_menu,:ps_menu,:nome_ps_menu,:ps_icone,:funcao_ps_menu,:ordem_ps_menu,:descricao_ps_menu,:ativo_ps_menu,:data_ps_menu,:hora_ps_menu,:ps_modulo,:ps_usuario)");
					$sql_set1->execute(array('id_uk_ps_menu' => $id_uk_ps_menu, 'ps_menu' => '', 'nome_ps_menu' => $nome_menu_ps_ref_tabs, 'ps_icone' => $ps_ref_tabs_ps_icone, 'funcao_ps_menu' => $funcao_ps_ref_tabs, 'ordem_ps_menu' => '1', 'descricao_ps_menu' => $descricao_ps_ref_tabs, 'ativo_ps_menu' => '1', 'data_ps_menu' => $data_ps_menu, 'hora_ps_menu' => $hora_ps_menu, 'ps_modulo' => $ps_modulo, 'ps_usuario' => $ps_usuario));
					$nome_menu_ps_ref_tabs = $id_uk_ps_menu;
					}
				}
			}
			
						
							

						
					
					
					$data_ps_ref_tabs = date('Y-m-d');
					
					$hora_ps_ref_tabs = date('H:i:s');
					//echo "aqui tp = ".$tp;
					if($tp == "2"){
					
					$nome_ps_menu = nome_ps_menu($nome_menu_ps_ref_tabs);
					
					
					$ps_icone = $nome_ps_menu['respo'][0]['ps_icone'];
					
					$funcao_ps_ref_tabs = $nome_ps_menu['respo'][0]['funcao_ps_menu'];
					
					$descricao_ps_ref_tabs = $nome_ps_menu['respo'][0]['descricao_ps_menu'];
					}

					$id_uk_perfil =	in_perfil($c_pdo);

					in_seg_perfil($c_pdo, $id_uk_perfil, $ps_modulo, $nome_menu_ps_ref_tabs);
					
					
					$sql_set = $c_pdo->prepare("INSERT INTO `ps_ref_tabs`(`id_ps_ref_tabs`,`id_uk_ps_ref_tabs`,`ps_conexoes`,`tabela_ps_ref_tabs`,`ps_modulo`,`nome_aplicacao_ps_ref_tabs`,`nome_menu_ps_ref_tabs`,`ps_icone`,`funcao_ps_ref_tabs`,`descricao_ps_ref_tabs`,`data_ps_ref_tabs`,`hora_ps_ref_tabs`,`ps_usuario`) VALUES (:id_ps_ref_tabs,:id_uk_ps_ref_tabs,:ps_conexoes,:tabela_ps_ref_tabs,:ps_modulo,:nome_aplicacao_ps_ref_tabs,:nome_menu_ps_ref_tabs,:ps_icone,:funcao_ps_ref_tabs,:descricao_ps_ref_tabs,:data_ps_ref_tabs,:hora_ps_ref_tabs,:ps_usuario)");

					$sql_set->execute(array('id_ps_ref_tabs' => $id_ps_ref_tabs, 'id_uk_ps_ref_tabs' => $id_uk_ps_ref_tabs, 'ps_conexoes' => $ps_conexoes, 'tabela_ps_ref_tabs' => $tabela_ps_ref_tabs, 'ps_modulo' => $ps_modulo, 'nome_aplicacao_ps_ref_tabs' => $nome_aplicacao_ps_ref_tabs, 'nome_menu_ps_ref_tabs' => $nome_menu_ps_ref_tabs, 'ps_icone' => $ps_icone, 'funcao_ps_ref_tabs' => $funcao_ps_ref_tabs, 'descricao_ps_ref_tabs' => $descricao_ps_ref_tabs, 'data_ps_ref_tabs' => $data_ps_ref_tabs, 'hora_ps_ref_tabs' => $hora_ps_ref_tabs, 'ps_usuario' => $ps_usuario));
				
				$nome_coluna_ps_ref_cols = explode(',',$nome_coluna_ps_ref_cols);
				$alias_ps_ref_cols = explode(',',$alias_ps_ref_cols);
				$ps_ref_tabs = $id_uk_ps_ref_tabs;
				$tipo_ps_ref_cols = explode(',',$tipo_ps_ref_cols);
				$ps_conexoes_fk = explode(',',$ps_conexoes_fk);
				$tabela_fk_ps_ref_cols = explode(',',$tabela_fk_ps_ref_cols);
				$coluna_fk_ps_ref_cols = explode(',',$coluna_fk_ps_ref_cols);
				$data_ps_ref_cols = $data_ps_ref_tabs;
				$hora_ps_ref_cols = $hora_ps_ref_tabs;
				$tipo_db_ps_ref_cols = explode(',',$tipo_db_ps_ref_cols);
				$tamanho_ps_ref_cols = explode(',',$tamanho_ps_ref_cols);																																			
				$req_ps_ref_cols = explode(',',$req_ps_ref_cols);

			if($sql_set){
				
				for($x=0;$x < sizeof($nome_coluna_ps_ref_cols);$x++){
					
					$id_uk_ps_ref_cols = validacao_uk('ps_ref_cols', 'id_uk_ps_ref_cols');
					
					cad_cols($id_uk_ps_ref_cols, $nome_coluna_ps_ref_cols[$x], $alias_ps_ref_cols[$x], $ps_ref_tabs, $tipo_ps_ref_cols[$x], $tabela_fk_ps_ref_cols[$x], $coluna_fk_ps_ref_cols[$x], $data_ps_ref_cols, $hora_ps_ref_cols, $ps_usuario, $tipo_db_ps_ref_cols[$x], $tamanho_ps_ref_cols[$x], $req_ps_ref_cols[$x], $ps_conexoes_fk[$x]);	
					
				}

			echo json_encode("INSERIDO COM SUCESSO");



			$sql_set = null;


			}



		}


	}



}



?>